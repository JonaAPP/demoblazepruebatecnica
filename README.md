# Prueba técnica.

### Descripción del proyecto:
Este respositorio se encuentran los dos proyectos relacionados al desarrollo de la prueba técnica, en el cuál dentro de la carpeta de nombre "PruebaTécnica" se podrá visualizar la automatización para un servicio GET y dentro de la carpeta de nombre "Demoblaze_test" se encuentran las automatizaciones para el front del portal Product Store.

### Anotaciones:

Ejecutar cada proyecto de forma individual, pues cada uno tiene sus respectivas configuraciones y dependencias las cuales se podrán ver en sus respectivos archivos Readme.md.
