package co.com.pruebatecnica.demoblaze.runners;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/demoblaze_test.feature",
        glue = "co.com.pruebatecnica.demoblaze.stepsdefinitions",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class DemoblazeTest {
}
