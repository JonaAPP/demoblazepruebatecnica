package co.com.pruebatecnica.demoblaze.stepsdefinitions;

import co.com.pruebatecnica.demoblaze.questions.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.Map;

import static co.com.pruebatecnica.demoblaze.userintefaces.HomePage.LAPTOP_CATEGORY;
import static co.com.pruebatecnica.demoblaze.userintefaces.LaptopCategory.FIRST_LAPTOP;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.hamcrest.CoreMatchers.equalTo;


public class DemoblazeTestStepsDefinitions {

    @Managed
    private WebDriver driver;

    @Before
    public void setUp(){
        OnStage.setTheStage(Cast.whereEveryoneCan(BrowseTheWeb.with(driver)));
        OnStage.theActorCalled("tester");
    }

    @Given("The user opens the navigator and enter into the webpage")
    public void theUserOpensTheNavigatorAndEnterIntoTheWebpage() {
        OnStage.theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com/index.html"));
    }
    @When("The user go to the laptop category and Click on item")
    public void theUserGoToTheLaptopCategoryAndClickOnItem(){
        WaitUntil.the(LAPTOP_CATEGORY, isVisible()).forNoMoreThan(40).seconds();
        OnStage.theActorInTheSpotlight().wasAbleTo(Click.on(LAPTOP_CATEGORY));
        WaitUntil.the(FIRST_LAPTOP, isVisible()).forNoMoreThan(40).seconds();
        OnStage.theActorInTheSpotlight().wasAbleTo(Click.on(FIRST_LAPTOP));
    }

    @When("The go to the laptop category")
    public void theGoToTheLaptopCategory() {
        WaitUntil.the(LAPTOP_CATEGORY, isVisible()).forNoMoreThan(40).seconds();
        OnStage.theActorInTheSpotlight().wasAbleTo(Click.on(LAPTOP_CATEGORY));
        WaitUntil.the(FIRST_LAPTOP, isVisible()).forNoMoreThan(40).seconds();
    }
    @Then("The user should validate the name of the product is correct")
    public void theUserShouldValidateTheNameOfTheProductIsCorrect(List<Map<String, String>> data) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidateProductName.productValidation(), equalTo(data.get(0).get("productName"))));
    }

    @Then("The user should validate the description of the product is correct")
    public void theUserShouldValidateTheDescriptionOfTheProductIsCorrect(List<Map<String, String>> data) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidateProductDescription.productValidation(), equalTo(data.get(0).get("productDescription"))));
    }

    @Then("The user should see the image name and description are visible")
    public void theUserShouldSeeTheImageNameAndDescriptionAreVisible() {
        WaitUntil.the(FIRST_LAPTOP, isVisible()).forNoMoreThan(40).seconds();
        OnStage.theActorInTheSpotlight().should(seeThat(ValidateCardImage.cardValidationImage(), Matchers.is(true)));
        OnStage.theActorInTheSpotlight().should(seeThat(ValidateCardName.cardValidationName(), Matchers.is(true)));
        OnStage.theActorInTheSpotlight().should(seeThat(ValidateCardDescription.cardValidationDescription(), Matchers.is(true)));

    }
}
