Feature: I as an user of the system
  I want to visualize the prices of the products in the laptops category
  to be able to make decisions about the purchase

  Background: The user enters to the webpage
    Given The user opens the navigator and enter into the webpage

  Scenario Outline: Consult the product name
    When The user go to the laptop category and Click on item
    Then The user should validate the name of the product is correct
      | productName   |
      | <productName> |
    Examples:
      | productName  |
      | Sony vaio i5 |

  Scenario Outline: Consult the product description
    When The user go to the laptop category and Click on item
    Then The user should validate the description of the product is correct
      | productDescription   |
      | <productDescription> |
    Examples:
      | productDescription                                                                                                                                                                                                               |
      | Sony is so confident that the VAIO S is a superior ultraportable laptop that the company proudly compares the notebook to Apple's 13-inch MacBook Pro. And in a lot of ways this notebook is better, thanks to a lighter weight. |
