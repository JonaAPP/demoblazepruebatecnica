package co.com.pruebatecnica.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pruebatecnica.demoblaze.userintefaces.LaptopCategory.*;

public class ValidateCardImage implements Question {
    @Override
    public Object answeredBy(Actor actor) {

        return CARD_IMAGE.resolveFor(actor).isDisplayed();
    }

    public static ValidateCardImage cardValidationImage(){return new ValidateCardImage();}
}
