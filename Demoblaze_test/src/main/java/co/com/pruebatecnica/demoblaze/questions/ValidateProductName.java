package co.com.pruebatecnica.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.pruebatecnica.demoblaze.userintefaces.ProductPage.PRODUCT_NAME;

public class ValidateProductName implements Question {


    @Override
    public Object answeredBy(Actor actor) {
        System.out.println(PRODUCT_NAME.resolveFor(actor).getText());
        return PRODUCT_NAME.resolveFor(actor).getText().trim();
    }

    public static ValidateProductName productValidation(){return new ValidateProductName();}
}
