package co.com.pruebatecnica.demoblaze.userintefaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LaptopCategory {

    public static final Target FIRST_LAPTOP = Target.the("First Laptop")
            .locatedBy("//a[contains(text(),'Sony vaio i5')]");

    public static final Target CARD_IMAGE = Target.the("Card Image")
            .located(By.xpath("//body/div[@id='contcont']/div[1]/div[2]/div[1]/div[2]/div[1]/a[1]/img[1]"));

    public static final Target CARD_DESCRIPTION = Target.the("Card Description")
            .locatedBy("/html[1]/body[1]/div[5]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/p[1]");
//
    public static final Target CARD_NAME = Target.the("Card name")
            .locatedBy("/html[1]/body[1]/div[5]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/h4[1]/a[1]");
}
