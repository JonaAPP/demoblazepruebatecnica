package co.com.pruebatecnica.demoblaze.userintefaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage {

    public static final Target LAPTOP_CATEGORY = Target.the("Laptop Category")
            .located(By.xpath("//a[contains(text(),'Laptops')]"));

}
