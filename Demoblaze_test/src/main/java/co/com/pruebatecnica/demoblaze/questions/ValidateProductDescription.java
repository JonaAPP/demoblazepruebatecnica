package co.com.pruebatecnica.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pruebatecnica.demoblaze.userintefaces.ProductPage.PRODUCT_DESCRIPTION;
import static co.com.pruebatecnica.demoblaze.userintefaces.ProductPage.PRODUCT_NAME;

public class ValidateProductDescription implements Question {

    @Override
    public String answeredBy(Actor actor) {
        return PRODUCT_DESCRIPTION.resolveFor(actor).getText();
    }

    public static ValidateProductDescription productValidation(){return new ValidateProductDescription();}
}
