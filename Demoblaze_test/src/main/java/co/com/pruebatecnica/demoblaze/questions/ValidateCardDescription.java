package co.com.pruebatecnica.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pruebatecnica.demoblaze.userintefaces.LaptopCategory.*;


public class ValidateCardDescription implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return CARD_DESCRIPTION.resolveFor(actor).isDisplayed();

    }

    public static ValidateCardDescription cardValidationDescription(){return new ValidateCardDescription();}
}
