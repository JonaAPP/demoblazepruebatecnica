package co.com.pruebatecnica.demoblaze.userintefaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ProductPage {

    public static final Target PRODUCT_NAME = Target.the("Product Name")
            .located(By.xpath("//h2[contains(text(),'Sony vaio i5')]"));

    public static final Target PRODUCT_DESCRIPTION = Target.the("Product Description")
            .locatedBy("//p[contains(text(),'Sony is so confident that the VAIO S is a superior')]");
}
