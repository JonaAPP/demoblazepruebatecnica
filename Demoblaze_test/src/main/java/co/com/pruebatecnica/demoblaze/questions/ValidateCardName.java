package co.com.pruebatecnica.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pruebatecnica.demoblaze.userintefaces.LaptopCategory.*;

public class ValidateCardName implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        return CARD_NAME.resolveFor(actor).isDisplayed();
    }

    public static ValidateCardName cardValidationName(){return new ValidateCardName();}
}
