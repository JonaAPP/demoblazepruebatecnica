# Prueba técnica.

### Descripción del proyecto:
Este proyecto se realiza con el objetivo de ejectuar pruebas en el endpoint que consume el api de geonames.org como pruebas técnica.

### Funcionalidades: 
Permite ejecutar pruebas automatizadas de caja negra para el consumo del api

### Dependencias:

1. [Serenity core 3.3.8](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-core/3.3.8)
2. [Serenity Screenplay 3.3.8](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-screenplay/3.3.8)
3. [Serenity Screenplay rest 3.3.8](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-screenplay-rest/3.3.8)
4. [Serenity Cucumber 3.3.8](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-cucumber/3.3.8)
5. [SLF4J API Module 2.0.3](https://mvnrepository.com/artifact/org.slf4j/slf4j-api/2.0.3)
6. [Lombok 1.18.24](https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.24)

Realizar instalación de las dependencias antes de ejecutar el entorno de prueba, adicional se realizó el uso de Java 11 y gradle en su versión 7.5.1

Además se utiliza JUnit para editar las configuraciones de ejecución de la prueba.