package co.com.pruebatecnica.certificacion.tasks;


import co.com.pruebatecnica.certificacion.interactions.Get;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;


import java.util.Map;

import static co.com.pruebatecnica.certificacion.utils.ConstantString.*;
import static co.com.pruebatecnica.certificacion.utils.ConstantString.SERVICE_SOURCE;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ExecuteGet implements Task {
    private final Map<String, String> data;

    public ExecuteGet(Map<String, String> data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        switch (data.get("requestType")) {
            case ALL_PARAMS:
            actor.attemptsTo(Get.resource(SERVICE_SOURCE).with(requestSpecification -> requestSpecification
                    .contentType(ContentType.JSON)
                    .relaxedHTTPSValidation()
                    .param(FORMATTED, data.get(FORMATTED))
                    .param(LATITUDE, data.get(LATITUDE))
                    .param(LONGITUDE, data.get(LONGITUDE))
                    .param(USERNAME, data.get(USERNAME))
                    .param(STYLE, data.get(STYLE))
            ));
            break;
            case NO_USER:
                actor.attemptsTo(Get.resource(SERVICE_SOURCE).with(requestSpecification -> requestSpecification
                        .contentType(ContentType.JSON)
                        .relaxedHTTPSValidation()
                        .param(FORMATTED, data.get(FORMATTED))
                        .param(LATITUDE, data.get(LATITUDE))
                        .param(LONGITUDE, data.get(LONGITUDE))
                        .param(STYLE, data.get(STYLE))
                ));
                break;
            default:
                break;
        }
    }
    public static ExecuteGet with(Map<String,String> data){
        return instrumented(ExecuteGet.class, data);
    }
}
